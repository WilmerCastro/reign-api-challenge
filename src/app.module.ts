import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './modules/config/config.module';
import {ConfigService} from "./modules/config/services/config.service";
import {Configuration} from "./modules/config/config.key";
import { DatabaseModule } from './database/database.module';
import { LoggerModule } from './modules/logger/logger.module';
import { QueueModule } from './modules/queue/queue.module';
import { TaskModule } from './modules/task/task.module';
import { SharedModule } from './modules/shared/shared.module';
import {ScheduleModule} from "nest-schedule";
import { PostedArticlesModule } from './modules/posted-articles/posted-articles.module';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
    ScheduleModule.register(),
    ConfigModule,
    DatabaseModule,
    LoggerModule,
    QueueModule,
    TaskModule,
    SharedModule,
    PostedArticlesModule,
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number | string;

  constructor(private readonly configService: ConfigService) {
    AppModule.port = this.configService.get(Configuration.PORT);
  }
}
