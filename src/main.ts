import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import * as basicAuth from 'express-basic-auth';
import {router} from 'bull-board';
import {ValidationPipe} from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  app.setGlobalPrefix('api');
  require('dotenv').config()

  const isDevelopmentEnv = process.env.NODE_ENV === 'development';

  if (isDevelopmentEnv) {
    const options = new DocumentBuilder()
        .setTitle('Reign-api challenge')
        .setDescription('Documentation for reign-api challenge')
        .build();

    const document = SwaggerModule.createDocument(app, options);

    app.use('/api/docs', basicAuth({
      challenge: true,
      users: { [process.env.API_DOCS_USER]: process.env.API_DOCS_PASSWORD },
    }));

    SwaggerModule.setup('/api/docs', app, document);

    app.use('/api/queue', basicAuth({
      challenge: true,
      users: { [process.env.API_QUEUE_USER]: process.env.API_QUEUE_PASSWORD },
    }));

    app.use('/api/queue', router);
  }

  await app.listen(AppModule.port);
}
bootstrap();
