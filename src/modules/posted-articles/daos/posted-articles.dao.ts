import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {IPostedArticles} from "../interfaces/posted-articles.interface";

@Injectable()
export class PostedArticlesDAO {
    constructor(
        @InjectModel('PostedArticles') private postedArticlesModel: Model<IPostedArticles>
    ) {}

    async getAll():Promise<IPostedArticles[]> {
        return this.postedArticlesModel.find();
    }

    async getById(id: string):Promise<IPostedArticles> {
        return this.postedArticlesModel.findById(id);
    }

    async create(postedArticle: IPostedArticles): Promise<IPostedArticles> {
        const postedArticleModel: IPostedArticles = new this.postedArticlesModel({
            ...postedArticle,
        });

        await postedArticleModel.save();
        return postedArticleModel;
    }

    async delete(id: string) {
        return this.postedArticlesModel.findByIdAndRemove(id);
    }

    async findAndModify(query, updatedPostedArticle) {
        return this.postedArticlesModel.findOneAndUpdate(query, updatedPostedArticle);
    }

    async filterPaginatedResult(query, options= {page: 1, limit: 5}) {
       // @ts-ignore
        return await this.postedArticlesModel.paginate(query, options);
    }

}