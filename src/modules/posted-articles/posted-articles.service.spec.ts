import {Test, TestingModule} from '@nestjs/testing';
import {PostedArticlesService} from './posted-articles.service';
import {SharedModule} from "../shared/shared.module";
import {HttpModule} from "@nestjs/axios";
import {getModelToken} from "@nestjs/mongoose";
import {PostedArticles} from "./schemas/posted-articles.schema";
import {PostedArticlesController} from "./posted-articles.controller";
import {PostedArticlesDAO} from "./daos/posted-articles.dao";
import winston from "winston";


describe('PostedArticlesService', () => {
  let service: PostedArticlesService;


  async function mockUserModel(dto: any) {

    this.data = dto;
    this.postedArticlesModel.findById = jest.fn();
    this.save  = () => {
      return this.data;
    };
  }

  function mockWinston() {

    const logger = {
      debug: jest.fn(),
      log: jest.fn(),
      error: jest.fn(),
    };
// IMPORTANT First mock winston
    jest.mock("winston", () => ({
      logger: jest.fn().mockReturnValue(logger),
      format: {
        colorize: jest.fn(),
        combine: jest.fn(),
        label: jest.fn(),
        timestamp: jest.fn(),
        printf: jest.fn()
      },
      createLogger: jest.fn().mockReturnValue(logger),
      transports: {
        Console: jest.fn()
      }
    }));
  }

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
        SharedModule,],
      controllers: [PostedArticlesController],
      providers: [
          PostedArticlesService,
          PostedArticlesDAO,
          {  provide: getModelToken('PostedArticles'),
          useValue: mockUserModel, },
        {provide: 'winston', useValue: mockWinston }],

      exports: [PostedArticlesDAO, PostedArticlesService]
    }).compile();

    service = module.get<PostedArticlesService>(PostedArticlesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return data from url', async () => {
    const expected = await service.getAlgoliaHackerNewsUrl();
    const result = expected.data.hits.length > 0;
    expect(result).toBeTruthy();
  });

  it('should filter posted article by month word', async () => {
    const postedArticles = {
      docs: [
        {postedArticleCreatedAt: new Date('2021-10-04T01:24:46.000Z')},
        {postedArticleCreatedAt: new Date('2021-10-04T00:55:58.000Z')},
        {postedArticleCreatedAt: new Date('2021-10-04T00:00:32.000Z')},
      ]
    }
    const expected = await service.filterPostedArticleByMonth(postedArticles, 'october');
    expect(expected).toBe(postedArticles);
  });

  it('should return true with equal month given',  () => {
    const expected = service.comparePostedArticleMonth(
        {postedArticleCreatedAt: new Date('2021-10-04T01:24:46.000Z')}, 'october');

    expect(expected).toBeTruthy();
  });

  it('should return false with different month given',  () => {
    const expected = service.comparePostedArticleMonth(
        {postedArticleCreatedAt: new Date('2021-10-04T01:24:46.000Z')}, 'january');

    expect(expected).toBeFalsy();
  });
});
