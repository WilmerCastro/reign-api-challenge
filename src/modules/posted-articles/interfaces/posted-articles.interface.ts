
export interface IPostedArticles {
    id?: string,
    postedArticleCreatedAt?: Date,
    title?: string,
    url?: string,
    author?: string,
    points?: string,
    storyText?: string,
    commentText?: string,
    numComments?: string,
    storyId?: string,
    storyTitle?: string,
    storyUrl?: string,
    parentId?: string,
    tags?: string[],
    objectId?: string,
    highlightResult?: any,

    save?();
    delete?();
}