import {HttpStatus, Inject, Injectable, NotFoundException} from '@nestjs/common';
import {PostedArticlesUrls} from "./keys/posted-articles-urls.keys";
import {HttpService} from "@nestjs/axios";
import {PostedArticlesDAO} from "./daos/posted-articles.dao";
import {IPostedArticles} from "./interfaces/posted-articles.interface";
import {ResponseMessage} from "../shared/keys/response-message.keys";
import {Logger} from "winston";
import {SharedService} from "../shared/shared.service";
import {QueryPostedArticlesDTO} from "./dtos/query-posted-articles.dto";

@Injectable()
export class PostedArticlesService {

    constructor(
        private httpService: HttpService,
        private readonly postedArticlesDAO: PostedArticlesDAO,
        private readonly sharedService: SharedService,
        @Inject('winston') protected readonly logger: Logger,
    ) {}


    async getAlgoliaHackerNewsUrl() {
        try {
            const response = await this.httpService.get(PostedArticlesUrls.ALGOLIA_HACKERS_NODEJS);
            return response.toPromise();
        } catch (e) {
            throw e;
        }
    }

    async importPostedArticles() {
        try {
            const hackersNews = await this.getAlgoliaHackerNewsUrl();

            for (const hackerNews of hackersNews.data.hits) {

                if (await this.postedArticlesDAO.findAndModify({objectId: hackerNews.objectID},
                    await PostedArticlesService.mappedAlgoliaHackerNewsObject(hackerNews))) { continue }

                await this.postedArticlesDAO.create(await PostedArticlesService.mappedAlgoliaHackerNewsObject(hackerNews));
            }

            return { response: ResponseMessage.DATA_IMPORTED }

        } catch (e) {
            this.logger.error( ResponseMessage.ERROR_IMPORTING_DATA, {meta: e} )
            throw e;
        }
    }

    async filterPostedArticles(filters: QueryPostedArticlesDTO) {
        try {

            const options = {
                // @ts-ignore
                page: filters.page ? parseInt(filters.page, 10): 1,
                // @ts-ignore
                limit: filters.limit ?  parseInt(filters.limit, 10): 5,
            }
            let month;

            delete filters.page;
            delete filters.limit;

            if (filters.author) {
                // @ts-ignore
                filters.author = { $regex: '.*' + filters.author + '.*' };
            }

            if (filters.tags) {
                // @ts-ignore
                filters.tags = { $all : filters.tags};
            }

            if (filters.title) {
                // @ts-ignore
                filters.title = { $regex: '.*' + filters.title + '.*' };
            }

            if (filters.month) {
                month = filters.month;
                delete filters.month;
            }

            const postedArticles = await this.postedArticlesDAO.filterPaginatedResult(filters, options);

            if (postedArticles.docs.length === 0) { throw new NotFoundException(ResponseMessage.POSTED_ARTICLES_NOT_FOUND)}

            if (month) { return await this.filterPostedArticleByMonth(postedArticles, month); }

            return postedArticles;
        } catch (e) {
            if (e.status !== HttpStatus.NOT_FOUND) {
                this.logger.error( ResponseMessage.ERROR_GETTING_POSTED_ARTICLES, {meta: e} )
            }
            throw e;
        }
    }

    async filterPostedArticleByMonth(postedArticles, month) {
        postedArticles.docs = postedArticles.docs.filter(postedArticle => {
            return this.comparePostedArticleMonth(postedArticle, month)
        });
        if (postedArticles.docs.length === 0) { throw new NotFoundException(ResponseMessage.POSTED_ARTICLES_NOT_FOUND)}
        return postedArticles;
    }

    comparePostedArticleMonth(postedArticle: IPostedArticles, month: string):boolean {
        return postedArticle.postedArticleCreatedAt.getMonth() === this.sharedService.monthWordToNumber(month);
    }

    async deletePostedArticle(id: string) {
        try {
            const postedArticle = await this.postedArticlesDAO.getById(id);

            if (!postedArticle) { throw new NotFoundException(ResponseMessage.POSTED_ARTICLES_NOT_FOUND)}

            await postedArticle.delete();
            return { response: ResponseMessage.POSTED_ARTICLE_DELETED}
        } catch (e) {
            throw e;
        }
    }

    private static async mappedAlgoliaHackerNewsObject(unmappedHackerNews): Promise<IPostedArticles> {
        return {
            postedArticleCreatedAt: unmappedHackerNews.created_at,
            title: unmappedHackerNews.tittle,
            url: unmappedHackerNews.url,
            author: unmappedHackerNews.author,
            points: unmappedHackerNews.points,
            storyText: unmappedHackerNews.story_text,
            commentText: unmappedHackerNews.comment_text,
            numComments: unmappedHackerNews.num_comments,
            storyId: unmappedHackerNews.story_id,
            storyTitle: unmappedHackerNews.story_title,
            storyUrl: unmappedHackerNews.story_url,
            parentId: unmappedHackerNews.parent_id,
            tags: unmappedHackerNews._tags,
            objectId: unmappedHackerNews.objectID,
            highlightResult: unmappedHackerNews._highlightResult,
        }
    }
}
