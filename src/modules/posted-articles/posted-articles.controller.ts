import {Controller, Delete, Get, Param, Query, UseGuards} from '@nestjs/common';
import {PostedArticlesService} from "./posted-articles.service";
import {AuthGuard} from "@nestjs/passport";
import {QueryPostedArticlesDTO} from "./dtos/query-posted-articles.dto";
import {IsAlphanumericDTO} from "./dtos/is-number-string.dto";
import {ApiBearerAuth, ApiNotFoundResponse, ApiOkResponse, ApiTags} from "@nestjs/swagger";
import {ResponseMessage} from "../shared/keys/response-message.keys";

@ApiTags('Posted Articles')
@ApiBearerAuth()
@Controller('posted-articles')
@UseGuards(AuthGuard('jwt'))
export class PostedArticlesController {

    constructor(
        private readonly postedArticlesService: PostedArticlesService,
    ) {}

    @Get()
    @ApiNotFoundResponse({description: ResponseMessage.POSTED_ARTICLES_NOT_FOUND})
    async getPostedArticles(@Query() query: QueryPostedArticlesDTO) {
        return this.postedArticlesService.filterPostedArticles(query)
    }

    @Get('import')
    @ApiOkResponse({description: ResponseMessage.DATA_IMPORTED})
    async importPostedArticles() {
        return this.postedArticlesService.importPostedArticles();
    }

    @Delete('/:id')
    @ApiOkResponse({description: ResponseMessage.POSTED_ARTICLE_DELETED})
    async deletePostedArticle(@Param() params: IsAlphanumericDTO) {
        return this.postedArticlesService.deletePostedArticle(params.id);
    }
}
