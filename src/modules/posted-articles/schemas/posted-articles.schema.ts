import * as Mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";


export const PostedArticles = new Mongoose.Schema({
    postedArticleCreatedAt: {
      type: Mongoose.Schema.Types.Date
    },
    title: {
        type: String,
    },
    url: {
        type: String,
    },
    author: {
        type: String,
    },
    points: {
        type: String,
    },
    storyText: {
        type: String,
    },
    commentText: {
        type: String,
    },
    numComments: {
        type: String,
    },
    storyId: {
        type: String,
    },
    storyTitle: {
        type: String,
    },
    storyUrl: {
        type: String,
    },
    parentId: {
        type: String,
    },
    tags: [String],
    objectId: {
        type: String,
        unique: true
    },
    highlightResult: {
        type: Mongoose.Schema.Types.Mixed,
    },
}, {timestamps: true});

PostedArticles.plugin(mongoosePaginate);