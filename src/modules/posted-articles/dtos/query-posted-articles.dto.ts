import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";
import {IsArray, IsNumberString, IsOptional, IsString} from "class-validator";

export class QueryPostedArticlesDTO {

    @IsNumberString()
    @IsOptional()
    @ApiProperty()
    @ApiPropertyOptional()
    page?: number;

    @IsNumberString()
    @IsOptional()
    @ApiProperty()
    @ApiPropertyOptional()
    limit?: number;

    @ApiProperty()
    @IsString()
    @IsOptional()
    @ApiPropertyOptional()
    author?: string;

    @ApiProperty()
    @IsArray()
    @IsOptional()
    @ApiPropertyOptional()
    tags?: [string];

    @IsString()
    @ApiProperty()
    @IsOptional()
    @ApiPropertyOptional()
    title?: string;

    @IsString()
    @ApiProperty()
    @IsOptional()
    @ApiPropertyOptional()
    month?: string;
}