import {ApiProperty} from "@nestjs/swagger";
import {IsAlphanumeric} from "class-validator";

export class IsAlphanumericDTO {

    @ApiProperty()
    @IsAlphanumeric()
    id: string;
}