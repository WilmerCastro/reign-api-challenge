import { Module } from '@nestjs/common';
import { PostedArticlesController } from './posted-articles.controller';
import { PostedArticlesService } from './posted-articles.service';
import {HttpModule} from "@nestjs/axios";
import {MongooseModule} from "@nestjs/mongoose";
import {PostedArticles} from "./schemas/posted-articles.schema";
import {PostedArticlesDAO} from "./daos/posted-articles.dao";
import {SharedModule} from "../shared/shared.module";

@Module({
  imports: [
      SharedModule,
      HttpModule,
      MongooseModule.forFeature([
      {name: 'PostedArticles', schema: PostedArticles}]),
  ],
  controllers: [PostedArticlesController],
  providers: [PostedArticlesService, PostedArticlesDAO],
  exports: [PostedArticlesDAO, PostedArticlesService]
})
export class PostedArticlesModule {}
