
import * as Mongoose from "mongoose";

export const Logger = new Mongoose.Schema({
    message: String,
    timestamp: Mongoose.Schema.Types.Mixed,
    level: String,
    meta: Mongoose.Schema.Types.Mixed,
})