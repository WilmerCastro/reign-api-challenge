import {WinstonModule} from "nest-winston";
import {ConfigModule} from "../config/config.module";
import {ConfigService} from "../config/services/config.service";
import {Configuration} from "../config/config.key";

const winston = require('winston');
require('winston-mongodb');

export const loggerProvider = [
    WinstonModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => ({
            transports: [
                new winston.transports.MongoDB({
                    db: configService.get(Configuration.MONGODB_URI),
                    options: {
                        poolSize: 2,
                        useNewUrlParser: true,
                        useUnifiedTopology: true},
                    metaKey: 'meta',
                })
            ]
        })
    })
]