import { Document } from "mongoose";

export interface ILogger extends Document{
    message?: string,
    timestamp?: any,
    level?: string,
    meta?: any,
}