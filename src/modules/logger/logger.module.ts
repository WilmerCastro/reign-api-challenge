import { Module } from '@nestjs/common';
import {loggerProvider} from "./logger.provider";

@Module({
  imports: [
      ...loggerProvider,
  ],
  exports: [...loggerProvider],
})
export class LoggerModule {}
