import { IsAlphanumeric } from 'class-validator';

export class ValidIdDTO {
    @IsAlphanumeric()
    id?: string;
}