import { Injectable } from '@nestjs/common';
import {MonthNumbers} from "./keys/month-numbers.keys";

@Injectable()
export class SharedService {

    monthWordToNumber(month: string) {
        return MonthNumbers[month.toUpperCase()];
    }
}
