import {BullQueueEvents, OnQueueActive, OnQueueEvent} from "nest-bull";
import {Job} from "bull";

export abstract class QueueProcessor {
    protected constructor(logger, queueName) {
        this.logger = logger;
        this.queueName = queueName;
    }
    protected logger;
    queueName;

    @OnQueueActive()
    onActive(job: Job) {
        this.logger.info(
            `${this.queueName}'s queue job of type ${job.name} is active`,
            {meta: job.returnvalue},
        );
    }

    @OnQueueEvent(BullQueueEvents.COMPLETED)
    onCompleted(job: Job) {
        this.logger.info(
            `${this.queueName}'s queue job of type ${job.name} has been completed`,
            {meta: job.returnvalue},
        );
    }
    @OnQueueEvent(BullQueueEvents.FAILED)
    onFailed(job: Job) {
        this.logger.error(
            `${this.queueName}'s queue job of type ${job.name} failed`,
            {meta: job.returnvalue},
        );
    }
    @OnQueueEvent(BullQueueEvents.ERROR)
    onError(job: Job) {
        console.log('', job);
        this.logger.error(
            `${this.queueName}'s queue job of type ${job.name} has an error`,
            {meta: job},
        );
    }

}

