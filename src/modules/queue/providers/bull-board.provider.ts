import {InjectQueue} from "nest-bull";
import {QueueName} from "../keys/queue.keys";
import {Queue} from "bull";
import {BullAdapter, setQueues} from "bull-board";

export class BullBoardProvider {

    constructor(
        @InjectQueue(QueueName.TASK) readonly taskQueue: Queue,
    ) {
        setQueues([
            new BullAdapter(this.taskQueue),
        ]);
    }
}