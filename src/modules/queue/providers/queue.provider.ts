import {QueueName} from "../keys/queue.keys";
import {BullModule} from "@nestjs/bull";

export const QueueProvider = BullModule.registerQueue({
    name: QueueName.TASK,
});
