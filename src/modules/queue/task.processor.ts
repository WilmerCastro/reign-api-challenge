import {QueueProcessor} from "./processor.interface";
import {Inject} from "@nestjs/common";
import {Logger} from "winston";
import {QueueName} from "./keys/queue.keys";
import {Process, Processor} from "nest-bull";
import {TaskQueueProcesses} from "./keys/task-queue-processes.keys";
import {DoneCallback, Job} from "bull";
import {PostedArticlesService} from "../posted-articles/posted-articles.service";

@Processor({name: QueueName.TASK})
export class TaskProcessor extends QueueProcessor {

    constructor(
        @Inject('winston') protected readonly logger: Logger,
        private readonly postedArticlesService: PostedArticlesService,
    ) {
        super(logger, QueueName.TASK);
    }
    @Process({name: TaskQueueProcesses.IMPORT_POSTED_ARTICLES})
    async processInsertPostedArticles(job: Job, callback: DoneCallback) {
        try {
            callback(null, await this.postedArticlesService.importPostedArticles());
        } catch (e) {
            callback(e);
        }
    }
}
