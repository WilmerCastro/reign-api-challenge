import { Module } from '@nestjs/common';
import {QueueProvider} from "./providers/queue.provider";
import {TaskProcessor} from "./task.processor";
import {BullBoardProvider} from "./providers/bull-board.provider";
import {LoggerModule} from "../logger/logger.module";
import {ConfigModule} from "../config/config.module";
import {PostedArticlesModule} from "../posted-articles/posted-articles.module";
import {ConfigService} from "../config/services/config.service";
import {BullModule} from "@nestjs/bull";
import {Configuration} from "../config/config.key";

@Module({
    imports: [
        BullModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                redis: {
                    host: configService.get(Configuration.REDIS_HOST),
                    port: +configService.get(Configuration.REDIS_PORT),
                },
            }),
            inject: [ConfigService],
        }),
        QueueProvider,
        LoggerModule,
        ConfigModule,
        PostedArticlesModule,
    ],
    providers: [
        TaskProcessor,
        BullBoardProvider
    ],
    exports: [BullModule, QueueProvider],
})
export class QueueModule {}
