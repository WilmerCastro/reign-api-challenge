import { Injectable } from '@nestjs/common';
import {ConfigService} from "../config/services/config.service";
import {Configuration} from "../config/config.key";
import {IUser} from "./interfaces/user.interface";

@Injectable()
export class UsersService {

    constructor(
        private readonly configService: ConfigService,
    ) {
    }

    private readonly users: IUser[] = [
        {
            userId: 1,
            username: this.configService.get(Configuration.USERNAME_TEST),
            password: this.configService.get(Configuration.PASSWORD_TEST),
        },
    ];

    async findOne(username: string): Promise<IUser | undefined> {
        return this.users.find(user => user.username === username);
    }

    async validatePassword(user: IUser, password: string) {
        return user.password === password;
    }
}
