import {Document} from "mongoose";

export interface ITask  extends Document {
    name?: string,
    cron?: string,
    startTime?: string,
    endTime?: string,
    maxRetry?: string,
    retryInterval?: number,
    processName?: string,
    queue?: string,
    processData?: any;
}