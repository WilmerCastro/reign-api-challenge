import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class TasksModel {

    @ApiProperty()
    name: string;

    @ApiProperty({
        description: 'Cron expression that determine periodicity of the tasks',
    })
    cron: string;

    @ApiProperty()
    startTime: string;

    @ApiProperty()
    endTime: Date;

    @ApiProperty({
        description: 'Maximum of retries allowed per tasks',
    })
    maxRetry: number;

    @ApiProperty({
        description: 'Time (ms) between each retry',
    })
    retryInterval: number;

    @ApiProperty({
        description: 'Process that will be execute it',
    })
    processName: string;

    @ApiPropertyOptional({
        description: 'Data ta may be need to executed',
    })
    processData: any;
}