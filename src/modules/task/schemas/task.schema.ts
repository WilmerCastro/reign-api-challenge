import * as Mongoose from "mongoose";

export const Task = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    cron: {
        type: String,
        required: true,
    },
    startTime: {
        type: Date,
    },
    endTime: {
        type: Date,
    },
    maxRetry: {
        type: Number,
    },
    retryInterval: {
        type: Number,
    },
    processName: {
        type: String,
    },
    queue: {
        type: String,
        required: true,
    },
    processData: {
        type: Mongoose.Schema.Types.Mixed,
        default: {},
    },
}, {timestamps: true})