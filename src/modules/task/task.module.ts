import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import {TaskDAO} from "./daos/task.dao";
import {MongooseModule} from "@nestjs/mongoose";
import {Task} from "./schemas/task.schema";
import {ConfigModule} from "../config/config.module";
import {QueueModule} from "../queue/queue.module";
@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Task', schema: Task},
    ]),
    ConfigModule,
    QueueModule,
  ],
  controllers: [TaskController],
  providers: [TaskService, TaskDAO],
  exports: [TaskDAO],
})
export class TaskModule {}
