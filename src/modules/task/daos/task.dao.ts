import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {ITask} from "../interfaces/task.interface";
import {Model} from "mongoose";
import {CreateTaskDTO} from "../dtos/create-task.dto";
import {UpdateTaskDTO} from "../dtos/update-task.dto";

@Injectable()
export class TaskDAO {
    constructor(
        @InjectModel('Task') private taskModel: Model<ITask>
    ) {}

    async getAll():Promise<ITask[]> {
        return this.taskModel.find();
    }

    async getById(id: string): Promise<ITask> {
        return await this.taskModel.findById(id).exec();
    }

    async create(task: CreateTaskDTO): Promise<ITask> {
        const taskModel: ITask = new this.taskModel({
            ...task,
        });

        await taskModel.save();
        return taskModel;
    }

    async delete(task: ITask) {
        // @ts-ignore
        return this.taskModel.deleteOne(task);
    }

    async update(id: string, updatedTask: UpdateTaskDTO) {
        // @ts-ignore
        return this.taskModel.updateOne({ _id: id }, updatedTask);
    }

    async find(params: object) {
        return this.taskModel.find(params);
    }

    async findOne(params: object) {
        return this.taskModel.findOne(params);
    }
}