import {IsNotEmpty, IsOptional, IsString} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {ResponseMessage} from "../../shared/keys/response-message.keys";
import {IsCron} from "../validators/is-cron.validator";

export class CreateTaskDTO {

    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    queue: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsCron(
        'cron',
        {
            message: ResponseMessage.CRON_INVALID,
        })
    cron: string;

    @ApiPropertyOptional()
    @IsOptional()
    startTime: Date;

    @ApiPropertyOptional()
    @IsOptional()
    endTime: Date;

    @ApiPropertyOptional()
    @IsOptional()
    maxRetry: number;

    @ApiPropertyOptional()
    @IsOptional()
    retryInterval: number;

    @ApiPropertyOptional()
    @IsNotEmpty()
    processName: string;

    @ApiPropertyOptional()
    @IsOptional()
    processData: any;
}