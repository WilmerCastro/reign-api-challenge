
import { IsOptional } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsCron } from '../validators/is-cron.validator';
import {ResponseMessage} from "../../shared/keys/response-message.keys";

export class UpdateTaskDTO {

    @ApiProperty()
    @IsOptional()
    name: string;

    @ApiProperty()
    @IsOptional()
    @IsCron(
        'cron',
        {
            message: ResponseMessage.CRON_INVALID,
        })
    cron: string;

    @ApiPropertyOptional()
    @IsOptional()
    startTime: Date;

    @ApiPropertyOptional()
    @IsOptional()
    endTime: Date;

    @ApiPropertyOptional()
    @IsOptional()
    maxRetry: number;

    @ApiPropertyOptional()
    @IsOptional()
    retryInterval: number;

    @ApiPropertyOptional()
    @IsOptional()
    processName: string;

    @ApiPropertyOptional()
    @IsOptional()
    processData: any;
}