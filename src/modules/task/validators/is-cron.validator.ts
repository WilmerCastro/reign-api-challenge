import {registerDecorator, ValidationOptions, ValidationArguments} from 'class-validator';
import { isValidCron } from 'cron-validator';

export function IsCron(property: string, validationOptions?: ValidationOptions) {

    return (object: object, propertyName: string) => {
        registerDecorator({
            name: 'IsCron',
            target: object.constructor,
            propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return typeof value === 'string' && isValidCron(value, {seconds: true});
                },
            },
        });
    };
}