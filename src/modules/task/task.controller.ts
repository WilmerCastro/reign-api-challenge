import {Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, UseGuards} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOkResponse, ApiTags} from "@nestjs/swagger";
import {ResponseMessage} from "../shared/keys/response-message.keys";
import {InjectQueue} from "nest-bull";
import {QueueName} from "../queue/keys/queue.keys";
import {Queue} from "bull";
import {TaskService} from "./task.service";
import {CreateTaskDTO} from "./dtos/create-task.dto";
import { UpdateTaskDTO } from './dtos/update-task.dto';
import {TasksModel} from "./models/task.models";
import {AuthGuard} from "@nestjs/passport";

@ApiTags('Tasks')
@ApiBearerAuth()
@Controller('task')
@UseGuards(AuthGuard('jwt'))
export class TaskController {

    constructor(
        @InjectQueue(QueueName.TASK) readonly tasksQueue: Queue,
        private readonly taskService: TaskService,
    ) {
    }

    @Get()
    @ApiOkResponse({type: [TasksModel]})
    async getAllItems() {
        return this.taskService.getTasks();
    }

    @Post()
    @HttpCode(201)
    @ApiBody({ type: CreateTaskDTO })
    async addItem(@Body() body: CreateTaskDTO) {
        return await this.taskService.createTask(body);
    }

    @Patch('/:id')
    @ApiBody({ type: UpdateTaskDTO })
    @ApiOkResponse({description: ResponseMessage.TASK_UPDATED})
    async update(@Param('id') id: string, @Body() body: UpdateTaskDTO) {
        return await this.taskService.updateTask({id, body});
    }

    @Delete('/:id')
    @ApiOkResponse({description: ResponseMessage.TASK_REMOVED})
    async removeItem(@Param('id') id: string) {
        return await this.taskService.removeTask(id);
    }

}
