import {Injectable, OnModuleInit} from '@nestjs/common';
import {InjectSchedule, NestSchedule, Schedule} from "nest-schedule";
import {TaskDAO} from "./daos/task.dao";
import {ITask} from "./interfaces/task.interface";
import {Setting} from "../config/keys/settings.keys";
import {SettingService} from "../config/services/setting.service";
import {QueueServices} from "../queue/keys/queue-services.keys";
import {ResponseMessage} from "../shared/keys/response-message.keys";
import {CreateTaskDTO} from "./dtos/create-task.dto";
import {QueueName} from "../queue/keys/queue.keys";
import {InjectQueue} from "nest-bull";
import {Queue} from "bull";

@Injectable()
export class TaskService extends NestSchedule implements OnModuleInit {

    constructor(
        @InjectSchedule() private readonly schedule: Schedule,
        @InjectQueue(QueueName.TASK) readonly taskQueue: Queue,
        private readonly taskDAO: TaskDAO,
        private readonly settingService: SettingService,
    ) {
        super();
    }


    async onModuleInit() {

        const tasks: ITask[] = await this.taskDAO.getAll();
        const timeout = await this.settingService.get(Setting.QUEUE_JOBS_TIMEOUT);

        for (const task of tasks) {
            this.schedule.scheduleCronJob(task.name, task.cron, async () => {
                await this[QueueServices[task.queue]].add(task.processName, task.processData, {
                    timeout,
                });
                return false;
            });
            console.warn(`Task ${task.name} scheduled, execution time: ${task.cron} `);
        }

    }

    async getTasks(): Promise<ITask[]> {
        return await this.taskDAO.getAll();
    }

    async createTask(taskRequest: CreateTaskDTO) {
        const task: ITask = await this.taskDAO.create(taskRequest);

        return {
            message: ResponseMessage.TASK_CREATED,
            task,
        };
    }

    async updateTask(updateRequest) {
        await this.taskDAO.update(updateRequest.id, updateRequest.body);

        const task: ITask = await this.taskDAO.getById(updateRequest.id);

        this.schedule.cancelJob(task.name);
        this.schedule.scheduleCronJob(task.name, task.cron, async () => {
            await this[QueueServices[task.queue]].add(task.processName, task.processData);
            return false;
        });

        return {
            message: ResponseMessage.TASK_UPDATED,
        };
    }

    async removeTask(id: string) {
        const task: ITask = await this.taskDAO.getById(id);
        await this.taskDAO.delete(task);

        return {
            message: ResponseMessage.TASK_REMOVED,
        };
    }
}
