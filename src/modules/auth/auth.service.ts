import {Injectable, UnauthorizedException} from '@nestjs/common';
import {UsersService} from "../users/users.service";
import {IJwtPayload} from "./interfaces/jwt-payload.interface";
import {ResponseMessage} from "../shared/keys/response-message.keys";
import {IUser} from "../users/interfaces/user.interface";
import {JwtService} from "@nestjs/jwt";

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService,
                private readonly jwtService: JwtService) {}

    async validateByJwt(jwtPayload: IJwtPayload): Promise<any> {
        const user = await this.usersService.findOne(jwtPayload.username);
        if (user && user.password === jwtPayload.password) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async createJwtPayload(data: IJwtPayload) {
        const jwt = this.jwtService.sign(data);

        return {
            expiresIn: '3600',
            token: jwt,
        }
    }

    async login(payload) {
        const user: IUser = await this.usersService.findOne(payload.username);

        if (!user) {
            throw new UnauthorizedException(ResponseMessage.WRONG_CREDENTIALS);
        }

        const passwordMatch = await this.usersService.validatePassword(user, payload.password);

        if (!passwordMatch) {
            throw new UnauthorizedException(ResponseMessage.WRONG_CREDENTIALS);
        }
        delete user.password;
        return { token: await this.createJwtPayload(user), user}
    }
}
