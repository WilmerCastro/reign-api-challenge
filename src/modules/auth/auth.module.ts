import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import {UsersModule} from "../users/users.module";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {ConfigModule} from "../config/config.module";
import {ConfigService} from "../config/services/config.service";
import {Configuration} from "../config/config.key";
import { AuthController } from './auth.controller';
import {JwtStrategy} from "./jwt.strategy";

@Module({
  imports: [UsersModule,
      ConfigModule,
    PassportModule.register({ session: false }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get(Configuration.SECRET_KEY_JWT),
        signOptions: {
          expiresIn: '3600',
        },
      }),
    }),],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
