


// tslint:disable-next-line:no-var-requires
import {Injectable, UnauthorizedException} from "@nestjs/common";
import {PassportStrategy} from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
import {ConfigService} from "../config/services/config.service";
import {AuthService} from "./auth.service";
import {IJwtPayload} from "./interfaces/jwt-payload.interface";
import {ResponseMessage} from "../shared/keys/response-message.keys";

require('dotenv').config();

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(private readonly configService: ConfigService, private authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true,
            secretOrKey: process.env.SECRET_KEY_JWT,
        });
    }

    async validate(payload: IJwtPayload) {
        const user = await this.authService.validateByJwt(payload);

        if (!user) {
            throw new UnauthorizedException(ResponseMessage.INVALID_TOKEN);
        }

        return user ;
    }
}