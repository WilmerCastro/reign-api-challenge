import {Body, Controller, Post} from '@nestjs/common';
import {AuthService} from "./auth.service";
import {LoginDTO} from "./dtos/login.dto";
import {ApiTags} from "@nestjs/swagger";

@Controller('auth')
@ApiTags('Auth')
export class AuthController {

    constructor(private readonly authService: AuthService) {
    }

    @Post()
    async login(@Body() body: LoginDTO) {
        return await this.authService.login(body);
    }
}
