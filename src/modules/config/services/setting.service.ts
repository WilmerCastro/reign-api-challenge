import { Injectable } from '@nestjs/common';
import {SettingDAO} from "../daos/setting.dao";
import {ISetting} from "../interfaces/setting.interface";
import {ResponseMessage} from "../../shared/keys/response-message.keys";
import {CreateSettingDTO} from "../dtos/create-setting.dto";

@Injectable()
export class SettingService {
    constructor(
        private readonly settingDAO: SettingDAO,
    ) {}

    async get(name: string ) {
        const setting: ISetting = await this.settingDAO.findOne({name});
        return setting ? setting.value : '';
    }

    async getSettings(): Promise<ISetting[]> {
        return await this.settingDAO.getAll();
    }

    async createSetting(SettingRequest: CreateSettingDTO) {
        const Setting: ISetting = await this.settingDAO.create(SettingRequest);

        return {
            message: ResponseMessage.SETTING_CREATED,
            Setting,
        };
    }

    async updateSetting(updateRequest) {
        await this.settingDAO.update(updateRequest.id, updateRequest.body);

        return {
            message: ResponseMessage.SETTING_UPDATED,
        };
    }

    async removeSetting(id: string | any) {
        const Setting: ISetting = await this.settingDAO.getById(id);
        await this.settingDAO.delete(Setting);

        return {
            message: ResponseMessage.SETTING_REMOVED,
        };
    }
}
