import { Test, TestingModule } from '@nestjs/testing';
import { SettingService } from './setting.service';
import {ConfigService} from "./config.service";
import {SettingDAO} from "../daos/setting.dao";
import {getModelToken} from "@nestjs/mongoose";
import {ResponseMessage} from "../../shared/keys/response-message.keys";

describe('SettingService', () => {
  let service: SettingService;

  function mockUserModel(dto: any) {
    this.data = dto;
    this.save  = () => {
      return this.data;
    };
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [    {
        provide: ConfigService,
        useValue: new ConfigService(),
      },
        SettingService,
        SettingDAO,
        {  provide: getModelToken('Setting'),
          useValue: mockUserModel, },],
    }).compile();

    service = module.get<SettingService>(SettingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a setting', async () => {
    const result = await service.createSetting({name: 'test', value: 10});
    const expected =  {message: ResponseMessage.SETTING_CREATED}
    expect(result.message).toBe(expected.message);
  });

});
