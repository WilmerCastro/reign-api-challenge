import * as fs from 'fs';
import {parse} from 'dotenv';

export class ConfigService {
    private readonly envConfig: {[key: string]: string};

    constructor() {

        const isDevelopmentEnv = typeof process.env.NODE_ENV === 'undefined' ||
            process.env.NODE_ENV === 'development';

        if (isDevelopmentEnv) {
            const envFilePath = `${process.cwd()}/.env`;
            const existPath = fs.existsSync(envFilePath);

            if (!existPath) {
                console.error(`.env file does not exist`);
                process.exit();
            }
            this.envConfig = parse(fs.readFileSync(envFilePath));
        } else {
            this.envConfig = {
                PORT: process.env.PORT,
            }
        }
    }

    get(key: string): string {
        return this.envConfig[key];
    }
}
