import {Document} from 'mongoose';

export interface ISetting extends Document {
    name: string,
    value: any,
}