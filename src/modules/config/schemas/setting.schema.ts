import * as Mongoose from 'mongoose';

export const Setting = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    value: {
        type: Mongoose.Schema.Types.Mixed,
        required: true,
    },
}, {timestamps: true });