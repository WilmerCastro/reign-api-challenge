import {Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, UseGuards} from '@nestjs/common';
import {ApiBearerAuth, ApiBody, ApiCreatedResponse, ApiOkResponse, ApiTags} from "@nestjs/swagger";
import {SettingService} from "./services/setting.service";
import {ResponseMessage} from "../shared/keys/response-message.keys";
import {Setting} from "./setting.model";
import {CreateSettingDTO} from "./dtos/create-setting.dto";
import {UpdateSettingDTO} from "./dtos/update-setting.dto";
import {ValidIdDTO} from "../shared/dtos/valid-id.dto";
import {AuthGuard} from "@nestjs/passport";

@Controller('setting')
@ApiTags('Settings')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class SettingController {

    constructor(
        private readonly settingService: SettingService,
    ) {}

    @Get()
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({ description: 'Setting\'s list', type: [Setting] } )
    async getAllItems() {
        return this.settingService.getSettings();
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @ApiBody({description: 'Create a setting', type: [CreateSettingDTO] })
    @ApiCreatedResponse({ description: ResponseMessage.SETTING_CREATED })
    async addItem(@Body() body: CreateSettingDTO) {
        return await this.settingService.createSetting(body);
    }

    @Patch('/:id')
    @ApiBody({description: 'Update a setting', type: [UpdateSettingDTO] })
    @ApiOkResponse({ description: ResponseMessage.SETTING_UPDATED })
    async update(@Param('id') id: string, @Body() body: UpdateSettingDTO) {
        return await this.settingService.updateSetting({id, body});
    }

    @Delete('/:id')
    @ApiOkResponse({ description: ResponseMessage.SETTING_REMOVED })
    async removeItem(@Param('id') id: ValidIdDTO) {
        return await this.settingService.removeSetting(id);
    }
}
