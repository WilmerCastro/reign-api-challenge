import { IsOptional } from 'class-validator';
import {  ApiProperty } from '@nestjs/swagger';

export class UpdateSettingDTO {

    @ApiProperty()
    @IsOptional()
    name: string;

    @ApiProperty()
    @IsOptional()
    value: string;
}