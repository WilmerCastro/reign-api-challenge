import { IsNotEmpty } from 'class-validator';
import {  ApiProperty } from '@nestjs/swagger';

export class CreateSettingDTO {

    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    value: string | any;
}