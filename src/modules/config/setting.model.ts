import {  ApiProperty } from '@nestjs/swagger';

export class Setting {
    @ApiProperty()
    name: string;

    @ApiProperty()
    value: any;
}