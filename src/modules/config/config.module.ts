import { Module } from '@nestjs/common';
import { ConfigService } from './services/config.service';
import { SettingService } from './services/setting.service';
import {MongooseModule} from "@nestjs/mongoose";
import {Setting} from "./schemas/setting.schema";
import { SettingController } from './setting.controller';
import {SettingDAO} from "./daos/setting.dao";

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Setting', schema: Setting},
    ]),
  ],
  providers: [
    {
      provide: ConfigService,
      useValue: new ConfigService(),
    },
    SettingService,
    SettingDAO
  ],
  exports: [ConfigService, SettingService],
  controllers: [SettingController]
})
export class ConfigModule {}
