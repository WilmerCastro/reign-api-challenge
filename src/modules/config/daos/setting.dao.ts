import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {ISetting} from "../interfaces/setting.interface";
import {CreateSettingDTO} from "../dtos/create-setting.dto";
import {UpdateSettingDTO} from "../dtos/update-setting.dto";

@Injectable()
export class SettingDAO {

    constructor(
        @InjectModel('Setting') private settingModel: Model<ISetting>,
    ) {}

    async getAll(): Promise<ISetting[]> {
        return this.settingModel.find({});
    }

    async getById(id: string): Promise<ISetting> {
        return await this.settingModel.findById(id).exec();
    }

    async create(setting: CreateSettingDTO): Promise<ISetting> {
        const settingModel: ISetting = new this.settingModel({
            ...setting,
        });

        await settingModel.save();
        return settingModel;
    }

    async delete(setting: ISetting | any) {
        return this.settingModel.deleteOne(setting);
    }

    async update(id: string, updatedSetting: UpdateSettingDTO) {
        return this.settingModel.updateOne({_id: id}, updatedSetting);
    }

    async find(params: object) {
        return this.settingModel.find(params);
    }

    async findOne(params: object) {
        return this.settingModel.findOne(params);
    }
}