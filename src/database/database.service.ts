import {MongooseModule} from "@nestjs/mongoose";
import {ConfigModule} from "../modules/config/config.module";
import {ConfigService} from "../modules/config/services/config.service";
import {Configuration} from "../modules/config/config.key";

export const databaseProviders = [
    MongooseModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (config: ConfigService) => (
            {
            uri: config.get(Configuration.MONGODB_URI),
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
    })
];