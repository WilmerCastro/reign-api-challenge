<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ docker-compose up dev
```

## Test

```bash
# unit tests
$ npm run test

```

## Start using the app

Steps to use the application:

1- You must go to log in to the endpoint: POST **{BaseUrl}/api/auth** (see documentation in swagger) to obtain a token, 
which you will use to be able to authenticate in the other endpoints, the credentials are in the .env file, called: **_USERNAME_TEST_** and **_PASSWORD_TEST_**.

2- Using the previous token as BearerToken (for this and the other endpoints), you must create a "setting" in the endpoint:
POST {BaseUrl} / api / setting (see documentation in swagger) to add the expiration time to the tasks , it should have this structure:

    {
        "name": "queue_jobs_timeout",
        "value": 120000
    }


The time (a number) can be more or less but the name must be like this.

3- creation of the cronjob: you must go to the endpoint: **POST {BaseUrl}/api/task** (see documentation in swagger) to create
the task that will execute the action to update the article posts, it must have this structure:



    {
        "name": "import_posted_articles",
        "cron": "0 * * * *",
        "processName": "import_posted_articles",
        "queue": "TASK"
    }


All data is mandatory, the cron time is: every hour, it can be changed, I recommend this page: https://crontab.guru/every-1-hour.

4 - When you have the setting and task ready, you must restart the server, you will get a message in the console that the cronjob was queued.

5- If you want to force the importation, you can use the endpoint: **GET {BaseUrl}/posted-articles/import**  (see documentation in swagger) with that done the 
information from posted articles about Node.js on Hacker News will be updated every hour!

## Documentation

Go to **{BaseUrl}/api/docs**

## Queue

To see the QueueProcess go to **{BaseUrl}/api/queue**



## License

Nest is [MIT licensed](LICENSE).
